#!/system/bin/sh
# BusyBox installer
# (c) 2015-2018 Anton Skshidlevsky <meefik@gmail.com>, GPLv3

SYSTEM_REMOUNT=$(busybox printf "$INSTALL_DIR" | busybox grep -c "^/system/")

if busybox test "$SYSTEM_REMOUNT" -ne 0
then
    busybox printf "Remounting /system to rw ... "
    busybox mount -o remount,rw /system
    if busybox test $? -eq 0
    then
        busybox printf 'done\n'
    else
        busybox mount -o remount,rw /system /system
        if busybox test $? -eq 0
        then
            busybox printf 'done 1\n'
        else
            /system/bin/mount -o remount,rw /system
            if busybox test $? -eq 0
            then
                busybox printf 'done 2\n'
            else
                /system/bin/mount -o remount,rw /system /system
                if busybox test $? -eq 0
                then
                    busybox printf 'done 3\n'
                else
                    busybox ln -s /dev/block/by-name/system /dev/root
                    if busybox test $? -eq 0
                    then
                        busybox mount -o rw,remount /
                        if busybox test $? -eq 0
                        then
                            busybox printf 'done 4\n'
                        else
                            busybox printf 'fail\n'
                            exit 1
                        fi
                    else
                        busybox printf 'fail\n'
                        exit 1
                    fi
                fi
            fi
        fi
    fi
fi


for fn in busybox ssl_helper andraxshell andraxzsh
do
    busybox printf "Copying $fn to $INSTALL_DIR ... "
    SOURCE_BIN=$(busybox which $fn)
    if busybox test -e "$INSTALL_DIR/$fn"
    then
        busybox rm "$INSTALL_DIR/$fn"
    fi
    busybox cp "$SOURCE_BIN" "$INSTALL_DIR/$fn"
    if busybox test $? -eq 0
    then
        busybox printf "DONE\n"
    else
        busybox printf "FAIL: Oh, God, UNINSTALL your previous busybox and try again...\n"
    fi

    busybox printf "Changing permissions for $fn ... "
    busybox chown 0:0 "$INSTALL_DIR/$fn"
    busybox chmod 755 "$INSTALL_DIR/$fn"
    if busybox test $? -eq 0
    then
        busybox printf "DONE\n"
    else
        busybox printf "FAIL\n"
    fi
done

if busybox test "$REPLACE_APPLETS" = "true"
then
    busybox printf "Removing old applets ... "
    #busybox --list | busybox xargs -I APPLET busybox rm $INSTALL_DIR/APPLET
    busybox --list | busybox grep -v busybox | while read fn
    do
        if busybox test -e "$INSTALL_DIR/$fn" -o -L "$INSTALL_DIR/$fn"
        then
            busybox rm "$INSTALL_DIR/$fn"
        fi
    done
    if busybox test $? -eq 0
    then
        busybox printf "DONE\n"
    else
        busybox printf "FAIL\n"
    fi
fi

if busybox test "$INSTALL_APPLETS" = "true"
then
    busybox printf "Installing new applets ... "
    "$INSTALL_DIR/busybox" --install -s "$INSTALL_DIR"
    if busybox test $? -eq 0
    then
        busybox printf "DONE\n"
    else
        busybox printf "FAIL\n"
    fi
fi


if busybox test "$SYSTEM_REMOUNT" -ne 0 -a -d /system/addon.d
then
    busybox printf "Installing addon.d script ... "
    echo "$INSTALL_DIR" > /system/addon.d/busybox-install-dir
    busybox chmod 644 /system/addon.d/busybox-install-dir
    busybox cp "$ENV_DIR/scripts/addon.d.sh" /system/addon.d/99-busybox.sh
    busybox chown 0:0 /system/addon.d/99-busybox.sh
    busybox chmod 755 /system/addon.d/99-busybox.sh
    if busybox test $? -eq 0
    then
        busybox printf "DONE\n"
    else
        busybox printf "FAIL\n"
    fi
fi

busybox printf 'Trying fucking up our fake su on /system/xbin/su '
if busybox test "$(busybox readlink /system/xbin/su)" = "/system/xbin/busybox"
    then
        busybox rm "/system/xbin/su"

        if busybox test $? -eq 0
        then
            busybox printf 'DONE\n'
        else
            busybox printf 'fail\n'
        fi
fi

busybox printf 'Puting our custom resolv.conf on the system '
busybox echo "# ANDRAX Custom resolver\n\nnameserver 208.67.222.222\nnameserver 208.67.220.220\n" > /system/etc/resolv.conf
if busybox test $? -eq 0
then
    busybox printf 'DONE\n'
else
    busybox printf 'fail\n'
fi


#if busybox test "$SYSTEM_REMOUNT" -ne 0
#then
#    busybox printf "Remounting /system to ro ... "
#    busybox mount -o ro,remount /system
#    if busybox test $? -eq 0
#    then
#        busybox printf "DONE\n"
#    else
#        busybox mount -o ro,remount /system /system
#        if busybox test $? -eq 0
#        then
#            busybox printf 'DONE\n'
#        else
#            busybox printf 'SKIP\n'
#            exit 1
#        fi
#    fi
#fi
